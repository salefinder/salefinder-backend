import { Router, Request, Response } from 'express';
import EmailService from '../../services/emails';

// Middleware
import HttpMiddleware from '../middlewares/http';

// Logging
import { logger } from '../../utils/logging';

const route = Router();

// All store information passes through here
export default (app) => {
    // Route appended to ex. /api/stores
    app.use('/emails', route);

    // Creates a new user with email
    route.post('/create', HttpMiddleware.basicPost, async (req, res) => {        

        // Missing email body parameter
        if (!('email' in req.body)) {
            return res.json({
                error: 'Could not add new email',
                reason: 'Missing email body parameter'
            }).status(400);
        }

        logger.debug(`Adding a new email`);

        // Get database connection
        const db = req.app.locals.db;
        const email = req.body.email;
        
        try {
            // Get product results from store
            await EmailService.addEmail(email, { dbConnection: db });
            return res.json().status(201);
        } catch(err) {
            logger.error(err);

            return res.json({
                error: 'Could not add new email',
                reason: 'There was an error while adding to database'
            }).status(200);
        }
    });
};