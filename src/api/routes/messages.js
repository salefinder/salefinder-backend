import { Router, Request, Response } from 'express';
import MessageService from '../../services/messages';

// Middleware
import HttpMiddleware from '../middlewares/http';

// Logging
import { logger } from '../../utils/logging';

const route = Router();

// All store information passes through here
export default (app) => {
    // Route appended to ex. /api/messages
    app.use('/messages', route);

    // Creates a new user with email
    route.post('/create', HttpMiddleware.basicPost, async (req, res) => {        

        // Missing email body
        if (!('email' in req.body)) {
            return res.json({
                error: 'Could not add new email',
                reason: 'Missing email body parameter'
            }).status(401);
        }

        // Missing email body
        if (!('message' in req.body)) {
            return res.json({
                error: 'Could not add new message',
                reason: 'Missing message body parameter'
            }).status(401);
        }

        logger.debug(`Adding a new email`);

        // Get database connection
        const db = req.app.locals.db;
        const { email, message } = req.body;
        
        try {
            // Get product results from store
            await MessageService.addMessage(email, message, { dbConnection: db });
            return res.json().status(201);
        } catch(err) {
            logger.error(err);

            return res.json({
                error: 'Could not add new message',
                reason: 'There was an error while adding to database'
            }).status(500);
        }
    });
};