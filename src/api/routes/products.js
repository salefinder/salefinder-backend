import { Router } from 'express';
import ProductService from '../../services/products';
import CacheService from '../../services/cache';

// Middleware
import HttpMiddleware from '../middlewares/http';

// Logging
import { logger } from '../../utils/logging';

const route = Router();

// All product information passes through here
export default (app) => {

    // Route appended to ex. /api/products
    app.use('/products', route);
    
    // Get products from a store
    route.get('/all', HttpMiddleware.basicGet, async (req, res) => {        
        logger.debug(`Getting all products`);

        // Pagination vars
        let limit;
        let offset;
        let sort;

        // Check any query params
        if (req.query) {

            // Set a limit
            if (!req.query.limit) {
                limit = 25;       
            } else {
                limit = parseInt(req.query.limit);
            }

            // Set offset
            if (!req.query.offset) {
                offset = 0
            } else {
                offset = parseInt(req.query.offset);
            }

            // Set sort
            if (!req.query.sort) {
                sort = 'random';
            } else {
                sort = req.query.sort;
            }
        }

        const queryParams = { limit, offset };

        try {
            // Check cache for all products
            logger.info('Checking cache for all items');
            const cacheAllProducts = await CacheService.getKeyValue('all_Items');
        
            // return results if we found them, null would be not found
            if (cacheAllProducts) {
                const remainingProducts = JSON.parse(cacheAllProducts).slice(offset, (offset + limit));

                return res.json({
                    allProducts: remainingProducts,
                    limit,
                    offset,
                    totalProducts: remainingProducts.length, 
                }).status(200);
            } else {
                logger.info('All items list not found in cache');
            };
        } catch (err) {
            logger.error(err.stack)
            return err;
        }
        
        // Get database connection
        const db = req.app.locals.db;
        
        try {
            // Get all products and count
            const totalProducts = await ProductService.countAllProducts('all_Items', { dbConnection: db });
            const allProducts = await ProductService.getProducts('all_Items', queryParams, { dbConnection: db });
            
            logger.info('Done getting all products!');
            
            // Save cache since we didn't find it before
            // CacheService.saveKeyValue('all_Items', allProducts);

            return res.json({
                allProducts,
                limit,
                offset,
                totalProducts, 
            }).status(200);

        } catch (err) {
            logger.error(err);

            return res.json({
                error: 'Could not get all products',
                reason: 'There was an error while getting all stores or products'
            }).status(500);
        };        
    });

    // Get products from a store
    route.get('/:store', HttpMiddleware.basicGet, async (req, res) => {

        // Get database connection
        const db = req.app.locals.db;
        const store = req.params.store;

        logger.debug(`Getting products from ${store}`);

        try {
            // Get product results from store
            const results = await ProductService.getProducts(store, { dbConnection: db });
        
            return res.json({
                store,
                results
            }).status(200);
        } catch (err) {
            logger.error(err);
            
            return res.json({
                error: 'Could not products for store',
                reason: 'There was an error while getting products'
            }).status(500);
        };
    });
};