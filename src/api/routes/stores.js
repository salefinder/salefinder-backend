import { Router, Request, Response } from 'express';
import StoreService from '../../services/stores';

import HttpMiddleware from '../middlewares/http';

// Logging
import { logger } from '../../utils/logging';

const route = Router();

// All store information passes through here
export default (app) => {
    // Route appended to ex. /api/stores
    app.use('/stores', route);

    // Get all stores on record
    route.get('/all', HttpMiddleware.basicGet, async (req, res) => {        
        logger.debug(`Getting all stores`);
        
        try {
            // Get database connection
            const db = req.app.locals.db;

            // Get product results from store
            const results = await StoreService.getAllStores({ dbConnection: db });
            
            return res.json({
                stores: results
            }).status(200);
        } catch(err) {
            logger.error(err);

            return res.json({
                error: 'Could not get all stores',
                reason: 'There was an error while getting all stores'
            }).status(200);
        }
    });
};