// Middleware for checking your requests
class HttpMiddleware {

    // Check post request
    async basicGet(req, res, next) {

        // Bad method
        if (req.method !== 'GET') {
            return res.json({
                error: 'Bad GET request',
                reason: 'Invalid request type for this route, must be GET'
            })
        }

        // Looks good
        return next();
    }

    // Check post request
    async basicPost(req, res, next) {

        // Bad method
        if (req.method !== 'POST') {
            return res.json({
                error: 'Bad POST request',
                reason: 'Invalid request type for this route, must be GET'
            })
        }

        const headers = req.headers;

        // Missing Content-Type header
        if (!('content-type' in headers)) {
            return res.json({
                error: 'Bad POST request',
                reason: 'Missing Content-Type header'
            }).status(400);
        }
        
        // Content-Type must be application/json
        if (headers['content-type'] !== 'application/json') {
            return res.json({
                error: 'Bad POST request',
                reason: 'Invalid Content-Type header, must be application/json'
            }).status(400);
        }

        // Missing body
        if (!req.body) {
            return res.json({
                error: 'Bad POST request',
                reason: 'Missing JSON body'
            }).status(400);
        }

        // Looks good
        return next();
    }
}

module.exports = new HttpMiddleware();