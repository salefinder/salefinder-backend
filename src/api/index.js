import { Router } from 'express';
import products from './routes/products';
import stores from './routes/stores';
import emails from './routes/emails';
import messages from './routes/messages';
import { json } from 'body-parser';

import { logger } from '../utils/logging';

export default () => {
    const app = Router();

    app.use(function (req, res, next) {
        const requestStart = Date.now();

        res.on("finish", () => {
            const { method, socket, url } = req;
            const { remoteAddress } = socket;
            const processingTime = Date.now() - requestStart;

            logger.info(`${method} ${url} ${remoteAddress} ${processingTime}ms`);
        });
        return next();
    });

    // Additional routes
    products(app);
    stores(app);
    emails(app);
    messages(app);

	return app
}