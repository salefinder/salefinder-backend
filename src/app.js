import config from './config';
import express from 'express';
import mongoConnection from './loaders/mongo';
import { logger } from './utils/logging';

async function startServer() {
    const app = express();

    // Connect to database
    const databaseConnection = await mongoConnection.createConnection();
    app.locals.db = databaseConnection 

    // Add defaults to http server
    await require('./loaders').default({ expressApp: app });

    // Tell http server to list to port
    app.listen(config.port, err => {
        if (err) {
            console.error(err);
            process.exit(1);
            return;
        }

        logger.info(`Server listening on port: ${config.port}`);
    });
}

startServer();