import dotenv from 'dotenv';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (envFound.error) {
    // This error should crash whole process
    throw new Error("Couldn't find .env file");
}

export default {
    // Express
    port: parseInt(process.env.EXPRESS_PORT, 10),

    // Database
    databaseURL: process.env.MONGODB_URI,
    databaseName: process.env.MONGODB_DATABASE,

    // Redis
    redisPort: process.env.REDIS_PORT,
    redisHost: process.env.REDIS_HOST,
    redisPassword: process.env.REDIS_PASSWORD,

    //API configs
    api: {
        prefix: '/api',
    },
};