import expressLoader from './express';
import { logger } from '../utils/logging';

export default async ({ expressApp }) => {
    // Setup express stuff
    expressLoader({ app: expressApp });
    logger.info('Express loaded');
};