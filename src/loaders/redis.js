import redis from 'redis';
import { logger } from '../utils/logging';
import config from '../config';

// Used for creating connection to mongo database
class RedisConnection {

    // Create connection
    async createConnection() {
        logger.debug('Redis - Connecting to cache...');
        
        try {
            // Connection client
            return redis.createClient({
                port: config.redisPort,
                host: config.redisHost,
                password: config.redisPassword,
            });
        } catch (err) {
            logger.error(err.stack)
            return err
        };
    };

    // Kill connection
    async closeConnection() {
    };
};

module.exports = new RedisConnection();