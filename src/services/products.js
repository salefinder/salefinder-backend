import { logger } from '../utils/logging';

// Handles any interactions with products database
class ProductService {

    // Get total of all products
    async countAllProducts(store, { dbConnection }) {
        if (!dbConnection) {
            return Error('Missing database connection');
        };

        try {
            logger.debug('Mongo - Getting count of items');
            const collection = dbConnection.collection(store);
            const count = await collection.countDocuments();

            logger.debug('Mongo - Got count of items');
            return count;
        } catch (err) {
            logger.error(err.stack);
            return err;
        };
    };

    // Get all products for a store
    async getProducts(store, queryParams, { dbConnection }) {
        if (!dbConnection) {
            return Error('Missing database connection');
        } else if (!store) {
            return Error('Missing store name, ex: all_Items, ulta, americanThreads');
        } else if (!queryParams) {
            return Error('Missing queryParams');
        };

        try {
            logger.debug('Mongo - Querying for products');
            const collection = dbConnection.collection(store);
            const { limit, offset } = queryParams;
            const results = await collection.find({}).skip(offset).limit(limit).toArray();
            logger.debug('Mongo - Query done');

            // Send back contents of table
            return results;
        } catch (err) {
            logger.error(err.stack);
            return err;
        };
    };
};

module.exports = new ProductService();