import { logger } from '../utils/logging';

// Handles any interactions with products database
class EmailService {

    // Get all products for a store
    async addEmail(emailAddress, { dbConnection }) {
        if (!dbConnection) {
            return Error('Missing database connection');
        }

        try {
            const collection = dbConnection.collection('emails');

            logger.debug('Mongo - Query adding email to collection');
            
            // Add email to DB
            const results = await collection.insertOne({
                email: emailAddress
            });

            logger.debug('Mongo - Query done');

            // Send back contents of table
            return results;
        } catch (err) {
            throw err;
        }
        
    }
}

module.exports = new EmailService();