import moment from 'moment'; // Time
import { logger } from '../utils/logging';

// Handles any interactions with contact messages
class MessageService {

    // Add a new contact message
    async addMessage(messageEmail, message, { dbConnection }) {
        if (!dbConnection) {
            return Error('Missing database connection');
        }

        try {
            const collection = dbConnection.collection('messages');

            logger.debug('Mongo - Query adding contact to collection');
            
            // Add message to DB
            const results = await collection.insertOne({
                time: moment().toISOString(), // Current time
                email: messageEmail,
                message,
            });

            logger.debug('Mongo - Contact message successfully added to messages collection');
            return results;
        } catch (err) {
            throw err;
        }
        
    }
}

module.exports = new MessageService();