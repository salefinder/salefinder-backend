import redisConnection from '../loaders/redis';
import { logger } from '../utils/logging';

// Cache for quick load time data
class CacheService {

    // Get a key
    async getKeyValue(key) {
        if (!key) {
            return Error('Missing key to lookup');
        };

        try {
            // Create redis connection
            const cacheConnection = await redisConnection.createConnection();
            logger.debug('Redis- Got connection!');

            // Look up key
            logger.debug(`Redis - Looking up key: ${key}`);
            
            return new Promise((resolve, reject) => {
                cacheConnection.get(key, (err, results) => {
                    if (err) {
                        reject(err);
                    };
    
                    logger.debug(`Redis - Got results for key: ${key}!`);
                    return resolve(results);
                });
            });
        } catch (err) {
            logger.error(err.stack);
            return err;
        };
    };

    // Save a key value pair to redis cache
    async saveKeyValue(key, value) {
        try {
            // Create redis connection
            logger.debug('Redis - Connecting to redis cache');
            const cacheConnection = await redisConnection.createConnection();
            
            logger.debug(value)
            // Save key value pair
            logger.debug(`Redis - Saving key: ${key}`);
            cacheConnection.set(key, JSON.stringify(value));

            logger.debug(`Redis: Key saved: ${key}`);
        } catch (err) {
            logger.error(err);
            return err;
        };
    };
}

module.exports = new CacheService();