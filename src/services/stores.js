import { logger } from '../utils/logging';

// Handles any interactions with products database
class StoreService {

    // Get all products for a store
    async getAllStores({ dbConnection }) {
        if (!dbConnection) {
            return Error('Missing database connection');
        }

        try {
            const collection = dbConnection.collection('stores');

            logger.debug('Mongo - Querying for stores');
            
            // Query results
            const results = await collection.find({}).toArray();

            logger.debug('Mongo - Query done');

            // Send back contents of table
            return results;
        } catch (err) {
            return err;
        }
    }
}

module.exports = new StoreService();